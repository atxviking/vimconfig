# vimconfig

My personal Vim configuration and setup process.

## Personal Vim Configuration

This is just how I've setup Vim on my systems. Feel free to use it too.
Pretty sure there's a better way to do this, but it works. So there's that.

## Instructions

-  Follow the instructions for installing __Maximum Awesome__ by *Square*.
  -  https://github.com/square/maximum-awesome
-  Replace each file with the ones in this repository
  -  `.gvimrc`
  -  `.vimrc.local`
-  Update plugins
  -  `vim`
  -  `VundleInstall`
